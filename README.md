# MDE-DevOps Example Docker Image CI-Pipeline

See the videos relating to this repository on YouTube, or the learning material on MDEnet.

## Quick start to using MDE-DevOps

This project contains examples of CI-pipeline scripts and Ant build files for automating execution of MDE tasks (Epsilon programs). An existing Docker Image that contains the Eclipse IDE and Epsilon is used to execute model management programs in a CI-pipeline.

## Epsilon project files (Playground example)

**metamodel.emf**, modelling language.

**model.flexmi**, model to be managed with programs.

**Graph_program.egl**, graphviz generation program.

**Java_program.egl**, Java code generation program.

**Validation_program.evl**, model validation program.

## Ant build example 

**build.xml**, show how 3 Playground build.xml files can be combined to execute 3 Epsilon programs.

## Simple Ant build approach with single Ant tasks

**.gitlab-ci.yml-single**, this version of the CI-pipeline script calls 3 different Ant build files (one per stage). Each Ant build file has a single task that executes an Epsilon program.

**Validation_Build.xml**, executes the model validation Epsilon program.

**Graph_Build.xml**, executes the Graphviz generation Epsilon program.

**Java_Build.xml**, executes the Java code generation Epsilon program.


## Monolithic Ant build approach

**.gitlab-ci.yml**, this version of the CI-pipeline script calls the Monolithic Ant build file which who execution path is controlled using parameters. Only one Ant build file needs to be maintained, however, it is slightly more complex as the execution context can be changed.

**Monolithic_Build.xml**, must be passed parameteres when called from the CI-pipline script. This Ant build file can execute any of the 3 Epsilon programs on a model depending on the parameters passed in.

